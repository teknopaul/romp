"use strict";

// connect publish 200 messages and exit

var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.connect();

function sleep(milliSeconds) {
    var startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds);
}

client.on('connected', function() {
    let msg = "Testing\n" + new Date() + "\n123\n"; 
    // for (let i = 0 ; i < 10000 ; i++) msg += i + ":123456789012345678901234567890123456789012345678901234567890\n";

    for (let i = 0 ; i < 200 ; i++) {
        client.send({
            'destination': 'memtop-a',
            'id' : 23,
            'body': msg,
            'foo': 'baa',
            'receipt' : 'send' + i
        });
        
    }
});

client.on('receipt', function(id) {
    //console.log("receipt received: " + id);
    if (id == 'send199') {
        client.disconnect();
    }
});

client.on('error', function(error_frame) {
    console.log(error_frame);
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
