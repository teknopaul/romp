var stomp = require('stomp');
var client = new stomp.Stomp({});

/*
 * Send empty header val, this was illegal in STOMP 1.0 but legal in 1.2
 */

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a'
    });
});

client.on('message', function(message) {
    if ( message.headers.quxx === "" ) {
        process.exit(0);
    }
});

client.on('error', function(error_frame) {
    console.log("error");
    process.exit(1);
});

client.connect();

var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write(
        "CONNECT\n" +
        "\n\0");

    setTimeout(function () {
        stompRaw.write(
            "SEND\n" +
            "destin"
        );
    }, 10);

    setTimeout(function () {
        stompRaw.write(
            "ation:memtop-a\n" +
            "receipt:send\n" +
            "foo:baa\n" +
            "quxx:\n" +
            "\n1\0"
        );
    }, 20);
});

stompRaw.on("frame", function(frame) {

    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") ) {
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
    }
    else {
        stompRaw.end();
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
