var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

/*
 * Multipacket sends should work as if it were one packet
 */
stompRaw.on("connect", function() {
    stompRaw.write(
        "CONN"
    );
    setTimeout(function() {
        stompRaw.write(
            "ECT\n" +
            "\n\0"
        );
    }, 50);
});

stompRaw.on("frame", function(frame) {

    if ( frame.startsWith("CONNECTED") ) {
        process.exit(0);
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        process.exit(1);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
},100);
