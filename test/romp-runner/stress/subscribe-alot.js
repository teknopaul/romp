"use strict";

const sys = require('util');
const stomp = require('stomp');

const stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'subscriber',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
};

var connected = 0;
/*
 * Subscribe and kill the TCP connection repeatedly (every 100ms)
 */
const createClient = function() {

    stomp_args.localAddress = '127.0.0.' + (Math.floor(Math.random() * 250) + 1)
    let client = new stomp.Stomp(stomp_args);

    client.on('connected', function() {
        client.subscribe({
            destination: 'memtop-a',
            id : '1',
            ack: 'client',
            receipt: 'sub' 
        });
        connected++;
        if ( connected % 100 == 0 ) console.log("connected " + connected);
    });

    client.on('message', function(message) {
        if ( client.ended ) {
            client.disconnect();
            return;
        }
        try {
            client.ack(message.headers['message-id']);
        } catch (err) {
            // write after end normal in this test
        }
    });

    client.on('error', function(error_frame) {
        //console.log("headers: " + sys.inspect(error_frame.headers));
        client.disconnect();
    });

    client.connect();

    return client;
}

setInterval(function() {
    let client = createClient();
    setTimeout(function() {
        client.ended = true;
        client.socket.end();
    }, 100 + (Math.random() * 100));
}, 100);

process.on('SIGINT', function() {
    process.exit(0);
});
