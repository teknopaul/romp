"use strict";

const StompRaw = require('stomp-raw').StompRaw;
const stompFrame = require('stomp-frame');
const stompRaw = new StompRaw();

/**
 * Pipelining commands, N.B. CONNECT is not allowed to be pipelined in current version.
 */
stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\n\n\0");
});

stompRaw.on("frame", function(frame) {
    if ( frame.startsWith("CONNECTED") ) {
        stompRaw.write("SUBSCRIBE\ndestination:memtop-a\nid:1\nack:client\nreceipt:sub\n\n\0");
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        console.log(stompFrame.parseStompMessage(frame));
        process.exit(1);
    }
    else if ( frame.startsWith("MESSAGE") ) {

        /**
         * This is the test: write two messages in a single packet.
         */
        let msg = stompFrame.parseStompMessage(frame);
        stompRaw.write("ACK\nid:" + msg.headers['message-id'] + "\n\n\0" +
                       "UNSUBSCRIBE\ndestination:memtop-a\nid:1\nreceipt:unsub\n\n\0");

    }
    else if ( frame.startsWith("RECEIPT") ) {
        let receipt = stompFrame.parseStompMessage(frame);
        if (receipt.headers['receipt-id'] === 'sub') {

            /**
             * Publisher
             */
            const stompRawPub = new StompRaw();
            stompRawPub.on("connect", function() {
                stompRawPub.write("CONNECT\n\n\0");
            });
            stompRawPub.on("frame", function(frame) {
                if ( frame.startsWith("CONNECTED") ) {
                    stompRawPub.write("SEND\ndestination:memtop-a\n\nyah boo sux!\n\0");
                }
            });

            setTimeout(() => stompRawPub.connect(), 25);
            setTimeout(() => stompRawPub.end(),     2000);

        }
        if (receipt.headers['receipt-id'] === 'unsub') {
            process.exit(0);
        }
    }
    else {
        console.log("unexpected frame");
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(() => {
    console.log("test timeout");
    stompRaw.end();
    process.exit(1);
}, 2000);
