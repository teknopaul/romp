#!/usr/bin/env node

var stomp = require('stomp');

var num = process.argv[2];

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.connect();

function doSend() {
    client.send({
        'destination': 'memtop-a',
        'body': "Testing\n" + new Date() + "\n123",
        'receipt' : 'send'
    });
}

client.on('connected', function() {
    client.sent = 0;
    doSend();
});

client.on('receipt', function(id) {
    client.sent++;
    setTimeout(doSend, 1000);
    //if ( client.sent % 1000 === 0) 
    console.log("sent: " + client.sent );
});

client.on('error', function(error_frame) {
    console.log(error_frame.headers);
    client.disconnect();
    setTimeout(function () {
        process.exit(1);
    }, 1000);
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
