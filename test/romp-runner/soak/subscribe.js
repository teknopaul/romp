var sys = require('util');
var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'subscriber',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
};

var messages = 0;
var CLIENTS = 1000;
var clients = [];

var createClient = function(id) {

    stomp_args.localAddress = '127.0.0.' + (Math.floor(Math.random() * 250) + 1)
    var client = new stomp.Stomp(stomp_args);
    client.id = id;

    clients.push(client);

    client.on('connected', function() {
        console.log('connected ' + client.id);
        client.subscribe({
            destination: 'memtop-a',
            id : '1',
            ack: 'client',
            receipt: 'sub' 
        });
    });

    client.on('receipt', function(id) {
    //    console.log('receipt received:' + id);
    });

    client.on('message', function(message) {
    //    console.log("headers: " + sys.inspect(message.headers));
    //    console.log("body: " + message.body);
        client.ack(message.headers['message-id']);
        messages++;
    });

    client.on('error', function(error_frame) {
        console.log("error");
        console.log("headers: " + sys.inspect(error_frame.headers));
    //    console.log("body: " + error_frame.body);
        client.disconnect();
    });

    client.connect();

//    setTimeout(function () {
//        client.socket.write('\n');
//    }, 55000);

}


process.on('SIGINT', function() {
    console.log('\nconsumed ' + messages + ' messages');
    for (var i = 0 ; i < CLIENTS ; i++) {
        clients[i].disconnect();
    }
});

for (var i = 0 ; i < CLIENTS ; i++ ) {
    createClient(i);
}
