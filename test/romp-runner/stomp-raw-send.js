var StompRaw = require('stomp-raw').StompRaw;

var stompRaw = new StompRaw({
    debug : false
});

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    console.log(frame);

    if ( frame.startsWith("CONNECTED") ) {
        stompRaw.write("SEND\ndestination:memtop-a\nreceipt:send\n\nmessage\0");
    }
    else if ( frame.startsWith("RECEIPT") ) {
        stompRaw.write("DISCONNECT\n\n\0");
    }
    else {
        console.log("ending normally");
        stompRaw.end();
    }
});

stompRaw.connect();
