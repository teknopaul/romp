#!/usr/bin/env node

var stomp = require('stomp');

var num = process.argv[2];

var stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.connect();

function sleep(milliSeconds) {
    var startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds);
}

client.on('connected', function() {
    client.send({
        'destination': 'memtop-a',
        'body': "Testing\n" + new Date() + "\n123",
        'receipt' : 'send'
    });
});

client.on('receipt', function(id) {
    console.log("receipt received: " + id);
    if (id === 'send') {
        client.disconnect();
    }
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
