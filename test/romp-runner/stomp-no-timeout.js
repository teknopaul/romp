"use strict";

const sys = require('util');
const stomp = require('stomp');

/*
 * Test connect do nothing and not timeout using the stomp-js module
 */
const stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'stomp',
    passcode: 'passcode',
    'heart-beat': '120000,120000'
};

const client = new stomp.Stomp(stomp_args);

const TIMEOUT = 120000;
var testDuration = 210000;
var timeout, interval;

client.connect();

client.on('connected', () => {
    console.log("connected");
    timeout = setTimeout( () => process.exit(1), testDuration + 1000);
    interval = setInterval( () => console.log("eta " + (testDuration -= 10000) + "ms"), 10000);
});

client.on('receipt', (receipt) => {
    if ( receipt === 'send' ) {
        console.log("Message received thus connection OK");
        process.exit(0);
    }
});

client.on('message', (message) => {
    console.log("Someone interrupting this test!!!");
});

client.on('error', (error_frame) => {
    console.error(error_frame.body);
    client.disconnect();
    process.exit(1);
});

setTimeout( () => {
    client.send({
        'destination' : 'memtop-a',
        'id' : 23,
        'body': 'Yey!',
        'receipt' : 'send'
    });
}, testDuration);

process.on('SIGINT', () => {
    if (timeout) clearTimeout(timeout);
    if (interval) clearTimeout(interval);
    client.disconnect();
});

