#!/usr/bin/env node

// remove messages from a queue from previous test

var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'xtomp',
    passcode: 'passcode',
};

var client = new stomp.Stomp(stomp_args);

client.on('connected', function() {
    client.subscribe({
        destination: 'memtop-a',
        id : '1'
    });
});

client.on('message', function(message) {
});

client.on('receipt', function(id) {
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});

client.connect();

setTimeout( () => {
    client.disconnect();
    process.exit(0);
}, 500);