"use strict";

/*
 * Test sending heat-beats, then a message, (test takes a long time)
 */

const StompRaw = require('stomp-raw').StompRaw;
const stompRaw = new StompRaw();



const TIMEOUT = 120000;
const HEART_BEAT_INTERVAL = 60000;
const testDuration = TIMEOUT + (TIMEOUT / 2);

var counter = testDuration;
setInterval( () => console.log("eta " + (counter -= 10000) + "ms"), 10000);


stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");

    setInterval( () => stompRaw.write("\n"), HEART_BEAT_INTERVAL);

    setTimeout( () => stompRaw.write("SEND\ndestination:memtop-a\nreceipt:send\n\nthe-message\0"), testDuration);

});

stompRaw.on("frame", function(frame) {
    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        process.exit(1);
    }
    else if ( frame.trim().startsWith("RECEIPT") ) {
        console.log("receipt received thus connection OK");
        process.exit(0);
    }
    else {
        console.log("unexpected frame: '" + frame + "'");
        process.exit(1);
    }
});

stompRaw.on("end", function () {
    console.log("we got stitched!");
    process.exit(1);
});

stompRaw.connect();

process.on('SIGINT', () => process.exit(0) );
