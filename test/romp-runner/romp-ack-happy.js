var sys = require('util');
var stomp = require('stomp');

/*
 * Send message to a subscriber with ACKs
 */
var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
};

var destination = "ackme";
var testMessage =  "Dont forget!\n";

var errorHandler = function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    setTimeout(function() {
        publisher.disconnect();
        subscriber.disconnect();
        process.exit(0);
    }, 100);
};

stomp_args.login = "sub1";
var subscriber = new stomp.Stomp(stomp_args);
stomp_args.login = "publisher";
var publisher = new stomp.Stomp(stomp_args);

// SUBSCRIBER - start

subscriber.on("connected", function() {
    subscriber.subscribe({
        destination: destination,
        id : "1",
        ack: "client",
        receipt: "sub"
    });
});

subscriber.on("receipt", function(id) {
    if (id === "sub") {
        subscriber.subscribed = 1;
    }
    else if (id === "unsub") {
        subscriber.disconnect();
        process.exit(0);
    }
    else console.error("unexpected receipt " + id);
});

subscriber.on("message", function(message) {
    if (testMessage !== "" + message.body) {
        console.error("wrong msg");
        console.error("want:" + testMessage);
        console.error("got:" + message.body);
        errorHandler({});
    }
    else {
        // console.log("on message: " + message.headers["message-id"]);
        subscriber.ack(message.headers["message-id"]);
        setTimeout( () => {
            subscriber.unsubscribe({
                destination: destination,
                id : "1",
                receipt: "unsub"
            });
        }, 10);

    }
});

subscriber.on("error", errorHandler);

// SUBSCRIBER1 - end

// PUBLISHER - start

var publisher = new stomp.Stomp(stomp_args);

publisher.on("connected", function() {

    if ( subscriber.subscribed === 1 ) {
        publisher.send({
            "destination" : destination,
            "body" : testMessage,
            "receipt" : "send"
        });
    }
    else {
        setTimeout(function() {
            publisher.send({
                "destination" : destination,
                "body" : testMessage,
                "receipt" : "send"
            });
        }, 100);
    }

});

publisher.on("receipt", function(id) {
    if (id === "send") {
        publisher.disconnect();
    }
});

publisher.on("error", errorHandler);

// PUBLISHER - end

process.on("SIGINT", function() {
    publisher.disconnect();
    subscriber.disconnect();
});


subscriber.connect();
publisher.connect();

setTimeout(function() {
    console.error("timeout: unexpected");
    publisher.disconnect();
    subscriber.disconnect();
    process.exit(1);
}, 2000);