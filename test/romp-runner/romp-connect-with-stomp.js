var StompRaw = require('stomp-raw').StompRaw;

/*
 * Test connecting with "STOMP" insted of "CONNECT".
 */
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("STOMP\nlogin:xtomp\npasscode:passcode\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    //console.log(frame);

    if ( frame.startsWith("CONNECTED") ) {
        //console.log("connected");
        stompRaw.end();
    }
    else {
        console.log("error");
        stompRaw.end();
    }
});

stompRaw.connect();
