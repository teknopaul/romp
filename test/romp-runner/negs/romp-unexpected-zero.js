var StompRaw = require('stomp-raw').StompRaw;

var stompRaw = new StompRaw({
    debug : false
});

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    //console.log(frame);

    if ( frame.startsWith("CONNECTED") ) {
        stompRaw.write("SEND\ndestination:memtop-a\nreceipt:send\n\nmessage\0");
    }
    else if ( frame.startsWith("RECEIPT") ) {
        // deliberate bug here missing \n\n server side should detect that we have not not finished parsing a frame when \0 arrived
        stompRaw.write("DISCONNECT\0");
    }
    else if ( frame.startsWith("ERROR") ) {
        // expected error
        stompRaw.end();
        process.exit(0);
    }
    else {
        stompRaw.end();
        process.exit(1);
    }
});

stompRaw.connect();
