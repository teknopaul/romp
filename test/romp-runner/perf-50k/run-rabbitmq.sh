#!/bin/bash -e
#
#
#
if [[ $(id -u) != "0" ]]
then
	echo "run as root"
	exit 1
fi

cd $(dirname $0)

../../xtomp-ngin/kill


echo "starting rabbitmq"
service rabbitmq-server start

msg_count=100

sleep 1
node subscribe $msg_count &

echo sleep 20
sleep 20
# rabbitmq does not like streamed sends for some reason?  maybe have to wait for reply first??
#node publish $msg_count &
for i in $(seq 1 $msg_count)
do 
	echo $i
	node publish 1
done
wait
