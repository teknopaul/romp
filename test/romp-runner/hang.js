var sys = require('util');
var stomp = require('stomp');

/*
 * Connect with heart-beat settings so this script never returns
 */

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "xtomp",
    passcode: "passcode",
    "heart-beat": "120000,120000",
};

var errorHandler = function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    subscriber.disconnect();
};


var subscriber = new stomp.Stomp(stomp_args);

// SUBSCRIBER - start

subscriber.connect();

subscriber.on("connected", function() {
    subscriber.subscribe({
        destination: "memtop-a",
        id : "1",
        ack: "client",
        receipt: "sub" 
    });
});

subscriber.on("receipt", function(id) {
    if (id === "sub") {
        subscriber.subscribed = true;
    }
    else console.error("unexpected receipt");
});

subscriber.on("message", function(message) {
    subscriber.ack(message.headers["message-id"]);
});

subscriber.on("error", errorHandler);

// SUBSCRIBER - end


process.on("SIGINT", function() {
    subscriber.disconnect();
    process.exit(0);
});


