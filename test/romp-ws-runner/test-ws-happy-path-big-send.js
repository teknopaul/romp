"use strict";


const net = require('net');
const assert = require('assert');

/**
 * Send a very big message over WS, max frame is 1048576
 */
let client = net.createConnection({host:"localhost", port: 61613}, () => {
	client.write("GET /xtomp HTTP/1.1\n" +
		"host: localhost\n" +
		"connection: upgrade\n" +
		"upgrade: websocket\n" +
		"origin: http://localhost\n" +
		"Sec-WebSocket-Version: 13\n" +
		"Sec-WebSocket-Protocol: stomp\n" +
		"Sec-WebSocket-Key: lCMmdU5K24A2vDkOYrqRLQ==\n\n");
});
client.on('data', (data) => {
	let string = data.toString();
	if ( string.indexOf('HTTP') === 0 ) {
		let status = string.split('\n')[0].trim();
		assert.equal(status, "HTTP/1.1 101 Switching Protocols");

		let buf = new Buffer(46);
		buf.writeUInt8(0x81, 0);
		buf.writeUInt8(0xa8, 1);   // len = 40
		buf.writeUInt32BE(0x0, 2); // noop mask
		buf.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0", 6);
		client.write(buf);

		setTimeout(() => {
			let buf = new Buffer(1048576 + 14);// Buffer.alloc(1048576 + 14, ".");
			buf.fill(".", 0);
			buf.writeUInt8(0x81, 0);
			buf.writeUInt8(0xff, 1);       // len = 127
			buf.writeUInt32BE(0x0, 2); // len = 0
			buf.writeUInt32BE(1048576, 6);// len = 1048576
			buf.writeUInt32BE(0x0, 10);    // noop mask
			buf.write("SEND\ndestination:memtop-a\ncontent-length:1048525\n\n", 14); //1048576 - 43 - 7 - 1
			// zero terminate
			buf.writeUInt8(0x0, 1048576 + 13);
			client.write(buf);

		}, 100);

		setTimeout(() => {
			if ( client.writable) process.exit(0);
		}, 200);

	}
	else {
		
	}
});
