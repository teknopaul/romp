//! Configuration code, for parsing server config `romp.toml`, logging config, and `romp.passwd` usernames.

extern crate toml;

// looks for config/config.rs
pub mod config;
pub mod log;
pub mod user;
