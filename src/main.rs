

use romp::prelude::*;

/// Romp server entry point
///
/// Custom filters can be added as follow...
///
/// ```
///
///     // Filter structs have an init() method
///     romp_stomp_filter(Box::new(PingFilter {}));
///
///     // Http Filter structs different error handling
///     romp_http_filter(Box::new(HelloFilter {}));
///
///     // STOMP filters can be written as closures
///     romp_stomp(|ctx, msg| {
///         if let Some(hdr) = msg.get_header("special-header") {
///             let mut session = ctx.session.write().unwrap();
///             session.send_message(get_response_ok("howdy".to_string()));
///             return Ok(true);
///         }
///         Ok(false)
///     });
///
///     // as can HTTP filters
///     romp_http(|ctx, msg| {
///         if "/pingerooni".eq(ctx.attributes.get("romp.uri").unwrap()) {
///             let mut session = ctx.session.write().unwrap();
///             session.send_message(Arc::new(get_http_ok_msg()));
///             return Ok(true);
///         }
///         Ok(false)
///     });
///     ///
///
/// ```
///
/// Then bootstrap the server
///
/// ```
/// tokio::run(bootstrap_romp());
/// ```

fn main() {

    tokio::run(romp_bootstrap());

}
