
use std::path::Path;

use log::*;

pub fn init_log4rs() {

    // TODO when this is released https://github.com/estk/log4rs/pull/73 make -v output debug level
    let mut log4rs_path = String::from("./conf");
    if let Ok(var) = std::env::var("ROMP_CONF") {
        log4rs_path = String::from(var.as_str());
    }
    log4rs_path.push_str("/romp-log4rs.yaml");

    if Path::new(log4rs_path.as_str()).exists() {
        log4rs::init_file(log4rs_path, Default::default()).unwrap();
        info!("loading...");
    } else if  Path::new("/etc/romp/romp-log4rs.yaml").exists() {
        log4rs::init_file(Path::new("/etc/romp/romp-log4rs.yaml"), Default::default()).unwrap();
        info!("loading...");
    }

}
