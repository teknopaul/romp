
use crate::init::CONFIG;

/// Return true if the provided port is considered trusted, this is port 61616 which
/// should never be exposed to the Internet.  By default this is bound only to `127.0.0.1`.
pub fn ws_is_trusted_port(port: u16) -> bool{
    CONFIG.admin_port == port
}

/// Returns true if the port is considered a web port 80, 81 8000-8099.
pub fn ws_is_web_port(port: u16) -> bool{
    80 == port || 81 == port || (8000 >= port && port <= 8099)
}