//! Handles SEND to `/virt/admin/clean`.

use crate::init::SERVER;
use crate::message::stomp_message::StompMessage;
use crate::message::stomp_message::StompCommand;
use crate::workflow::context::Context;
use crate::workflow::filter::{AdminFilter, FilterError};
use crate::util::some_string;
use crate::message::response::get_response_ok;
use crate::workflow::{CONTINUE, HANDLED};

pub struct CleanFilter {
}

impl CleanFilter {
    pub fn new()-> CleanFilter {
        CleanFilter {
        }
    }
}

impl AdminFilter for CleanFilter {

    fn init(&mut self) {
    }

    fn do_filter(&self, context: &mut Context, message: &mut StompMessage) -> Result<bool, FilterError> {
        if StompCommand::Send == message.command {
            if let Some(destination_name) = message.get_header("destination") {
                if "/virt/admin/clean".eq(destination_name) {
                    if let Some(destination_to_clean) = message.get_header("name") {
                        let destination = SERVER.find_destination(destination_to_clean);
                        return match destination {
                            None => Err(FilterError {
                                reply_message: some_string("destination unknown"),
                                log_message: some_string("destination unknown"),
                                fatal: false
                            }),
                            Some(destination) => {
                                let cleaned = destination.write().unwrap().clean();
                                context.session.read().unwrap().send_message(get_response_ok(format!("cleaned: {}", cleaned)));
                                Ok(HANDLED)
                            }
                        }
                    }
                }
            }
        }

        Ok(CONTINUE)
    }

}