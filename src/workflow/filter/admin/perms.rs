//! Handles SEND to `/virt/admin/perms`, setting permissions for a destination without a reboot.
//! Settings are not saved.

use crate::init::SERVER;
use crate::message::stomp_message::StompMessage;
use crate::message::stomp_message::StompCommand;
use crate::workflow::context::Context;
use crate::workflow::filter::{AdminFilter, FilterError};
use crate::util::some_string;
use crate::workflow::destination::destination::RuntimeConfig;
use crate::workflow::CONTINUE;


pub struct PermsFilter {
}

impl PermsFilter {
    pub fn new()-> PermsFilter {
        PermsFilter {
        }
    }
}

impl AdminFilter for PermsFilter {

    fn init(&mut self) {
    }

    fn do_filter(&self, _context: &mut Context, message: &mut StompMessage) -> Result<bool, FilterError> {
        if StompCommand::Send == message.command {
            if let Some(destination_name) = message.get_header("destination") {
                if "/virt/admin/perms".eq(destination_name) {
                    if let Some(destination_to_mod) = message.get_header("name") {
                        let destination = SERVER.find_destination(destination_to_mod);
                        match destination {
                            None => return Err(FilterError {
                                reply_message: some_string("destination unknown"),
                                log_message: some_string("destination unknown"),
                                fatal: false
                            }),
                            Some(destination) => {
                                let mut cfg = RuntimeConfig { ..Default::default()};
                                if message.get_header("max_connections").is_some() {
                                    cfg.max_connections = message.get_header("max_connections").unwrap().parse::<usize>().ok();
                                }
                                if message.get_header("max_messages").is_some() {
                                    cfg.max_messages = message.get_header("max_messages").unwrap().parse::<usize>().ok();
                                }
                                if message.get_header("expiry").is_some() {
                                    cfg.expiry = message.get_header("expiry").unwrap().parse::<u64>().ok();
                                }
                                if message.get_header("max_message_size").is_some() {
                                    cfg.max_message_size = message.get_header("max_message_size").unwrap().parse::<usize>().ok();
                                }
                                if message.get_header("write_block").is_some() {
                                    cfg.write_block = message.get_header("write_block").unwrap().parse::<bool>().ok();
                                }
                                if message.get_header("read_block").is_some() {
                                    cfg.read_block = message.get_header("read_block").unwrap().parse::<bool>().ok();
                                }
                                if message.get_header("web_write_block").is_some() {
                                    cfg.web_write_block = message.get_header("web_write_block").unwrap().parse::<bool>().ok();
                                }
                                if message.get_header("web_read_block").is_some() {
                                    cfg.web_read_block = message.get_header("web_read_block").unwrap().parse::<bool>().ok();
                                }
                                let mut destination = destination.write().unwrap();
                                destination.reconfigure(cfg);
                            }
                        }
                    }
                }
            }
        }

        Ok(CONTINUE)
    }

}