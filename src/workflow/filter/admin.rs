pub mod clean;
pub mod perms;
pub mod shutdown;

use crate::prelude::*;

use crate::init::CONFIG;
use crate::workflow::admin_router::route_admin;
use crate::util::some_string;

/// handles STOMP messages for administration of the app
///
#[derive(Debug)]
pub struct AdminFilter {
}

impl AdminFilter {

    pub fn new()-> AdminFilter {
        AdminFilter {
        }
    }

}

impl MessageFilter for AdminFilter {

    fn init(&mut self) {
    }

    fn do_filter(&self, context: &mut Context, message: &mut StompMessage) -> Result<bool, FilterError> {
        if ! CONFIG.enable_admin {
            return Ok(CONTINUE);
        }
        if StompCommand::Send == message.command {
            if let Some(destination_name) = message.get_header("destination") {
                if destination_name.starts_with("/virt/admin/") {
                    if ! context.is_admin() {
                        return Err(FilterError {
                            reply_message: some_string("auth err"),
                            log_message: some_string("blocked admin access"),
                            fatal: true
                        });
                    }
                    return match route_admin(context, message) {
                        Ok(cont) => Ok(cont),
                        Err(admin_err) => Err(admin_err),
                    }
                }
            }
        }

        Ok(CONTINUE)
    }

}