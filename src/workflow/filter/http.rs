
pub mod closure;
pub mod get;
pub mod health_check;
pub mod hello;
pub mod not_found;
pub mod upgrade;
pub mod version;

use crate::prelude::*;

use crate::workflow::http_router::route_http;
use crate::message::stomp_message::MessageType;

/// handles HTTP messages, currently only GETs, used for health checks of the server and Upgrading HTTP to STOMP over websockets
///
#[derive(Debug)]
pub struct HttpFilter {
}

impl HttpFilter {

    pub fn new()-> HttpFilter {
        HttpFilter {
        }
    }

}

impl MessageFilter for HttpFilter {

    fn init(&mut self) { }

    fn do_filter(&self, context: &mut Context, message: &mut StompMessage) -> Result<bool, FilterError> {
        match &message.message_type {
            MessageType::Http => {
                return match route_http(context, message) {
                    Ok(_) => {
                        Ok(HANDLED)
                    },
                    Err(_) => {
                        Err(FilterError {
                            reply_message: None,
                            log_message: None,
                            /// All HTTP errors are fatal
                            fatal: true
                        })
                    }
                }
            },
            _ => {}
        }

        Ok(CONTINUE)
    }

}