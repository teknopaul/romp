//! Contains canned responses.

use std::sync::Arc;

use crate::errors::*;
use crate::message::stomp_message::*;
use crate::message::stomp_message::StompCommand;
use crate::message::stomp_message::MessageType::Http;

pub const SERVER_TAG: &str = "romp/0.4";

/// Creates reply messages
///
/// TODO could optimize some of this to static strings
/// TODO should have C style function prefixes e.g. msg_xxx

pub fn get_response_connected(ecg_hdr: Header) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Connected;
    message.add_header("server", "romp");
    message.add_header("version", "1.0");
    message.push_header(ecg_hdr);
    Arc::new(message)
}

pub fn get_response_error_general() -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Error;
    message.add_header("message", "general");
    Arc::new(message)
}

pub fn get_response_error_server(_err: ServerError) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Error;
    message.add_header("message", "general");
    Arc::new(message)
}

pub fn get_response_error(text: &str) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Error;
    message.add_header_clone("message", text);
    Arc::new(message)
}

pub fn get_response_receipt(id: &str) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Receipt;
    message.add_header_clone("receipt-id", id);
    Arc::new(message)
}

pub fn get_response_error_client(err: ClientError) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Error;
    match err {
        ClientError::Syntax => {
            message.add_header("message", "syntax");
        }
        ClientError::BodyFlup => {
            message.add_header("message", "msg flup");
        }
        ClientError::HdrFlup => {
            message.add_header("message", "hdr flup");
        }
        ClientError::Timeout => {
            message.add_header("message", "timeout");
        }
        ClientError::SubsFlup => {
            message.add_header("message", "subs flup");
        }

    }
    Arc::new(message)
}

// STONP message with message header and no body
pub fn get_response_ok(text: String) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Message;
    message.add_header_clone("message", text.as_str());
    message.add_body("\n".as_bytes());
    Arc::new(message)
}

/// Get destination statistics message
pub fn get_response_stats_msg(destination_name: &str, size: usize, q_size: usize, delta: usize, total: usize) -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Message;
    message.add_header("destination", "/xtomp/stat");
    message.add_body(format!("{{\n  \"dest\":\"{}\",\n  \"sz\":{},\n  \"q\":{},\n  \"Δ\":{},\n  \"Σ\":{}\n}}\n", destination_name, size, q_size, delta, total).as_bytes());
    message
}

/// Get concurrent_connections statistics message
pub fn get_response_cc_msg(concurrent_connections: usize, uptime: i64, total_connections: usize, total_messages: usize) -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Message;
    message.add_header("destination", "/xtomp/stat");
    message.add_body(format!("{{\n  \"cc\":{{\n    \"sz\":{},\n    \"up\":{},\n    \"Σ\":{},\n    \"Σµ\":{},\n    \"m\":{}\n  }}\n}}\n", concurrent_connections, uptime, total_connections, total_messages, 0).as_bytes());
    message
}


// HTTP

/// Get empty OK message
pub fn get_http_ok_msg() -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::Ok;
    message.add_header("Access-Control-Allow-Origin", "*");
    message.add_header("Access-Control-Expose-Headers", "server");
    message.add_header("server", SERVER_TAG);
    message.add_header("content-length", "0");
    message
}

/// Get text OK message
pub fn get_http_ok_text_content(text: String, content_type: &'static str) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::Ok;
    message.add_header("Access-Control-Allow-Origin", "*");
    message.add_header("Access-Control-Expose-Headers", "server");
    message.add_header("server", SERVER_TAG);
    let bytes = text.as_bytes();
    message.push_header(Header::from_string("content-length", bytes.len().to_string()));
    message.add_header("content-type", content_type);
    message.add_body(bytes);
    Arc::new(message)
}

pub fn get_http_err_msg() -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::ServerError;
    message.add_header("server", SERVER_TAG);
    message
}

pub fn get_http_404_msg() -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::NotFound;
    message.add_header("server", SERVER_TAG);
    message
}

pub fn get_http_503_msg() -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::ServiceUnavailable;
    message.add_header("server", SERVER_TAG);
    message
}

pub fn get_http_client_err_msg() -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::ClientError;
    message.add_header("server", SERVER_TAG);
    message
}

pub fn get_http_upgrade_msg(websocket_accept: &String) -> StompMessage {
    let mut message = StompMessage::new(Ownership::Session);
    message.message_type = Http;
    message.command = StompCommand::Upgrade;
    message.add_header("Upgrade", "websocket");
    message.add_header("Connection", "Upgrade");
    message.add_header("Sec-WebSocket-Protocol", "stomp");
    message.add_header_clone("Sec-WebSocket-Accept", websocket_accept);
    message
}