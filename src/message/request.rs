use crate::config::config::Downstream;
use std::sync::Arc;
use crate::message::stomp_message::{StompMessage, Ownership, StompCommand, Header};


// TODO could cache this for reconnects
pub fn get_request_connect(downstream: &Downstream) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Connect;
    if let Some(_login) = &downstream.login {
        message.push_header(Header {
            name: "login".to_string(),
            value: downstream.login.as_ref().unwrap().clone()
        });
        message.push_header(Header {
            name: "passcode".to_string(),
            value: downstream.passcode.clone()
        });
        message.push_header(Header {
            name: "receipt".to_string(),
            value: format!("con|{}", &downstream.name),
        });
        // TODO heart-beat
    }
    Arc::new(message)
}

// TODO could cache this for reconnects
pub fn get_request_subscribe(destination: &String, downstream: &Downstream) -> Arc<StompMessage> {
    let mut message = StompMessage::new(Ownership::Session);
    message.command = StompCommand::Subscribe;
    if let Some(_login) = &downstream.login {
        message.push_header(Header {
            name: "destination".to_string(),
            value: destination.clone()
        });
        message.push_header(Header {
            name: "receipt".to_string(),
            value: format!("sub|{}|{}", &downstream.name, destination),
        });
    }
    Arc::new(message)
}
