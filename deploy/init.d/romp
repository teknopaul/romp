#! /bin/sh
### BEGIN INIT INFO
# Provides:          romp
# Required-Start:    $remote_fs $time
# Required-Stop:     umountnfs $time
# X-Stop-After:      sendsigs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: A STOMP/WebSockets server
# Description:
### END INIT INFO

#
# Author: teknopaul <teknopaul@tp23.org>
#

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="stomp server"
NAME=romp
DAEMON=/usr/sbin/romp
PIDFILE=/run/romp/romp.pid
SCRIPTNAME=/etc/init.d/$NAME

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Define LSB log_* functions.
. /lib/lsb/init-functions

do_start()
{
	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   other if daemon could not be started or a failure occured
	mkdir -p /run/romp
	start-stop-daemon --start --quiet --background --pidfile $PIDFILE --exec $DAEMON
}

do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   other if daemon could not be stopped or a failure occurred
	echo "" | xtomp-cat -q /virt/admin/shutdown -p 61616
	sleep 60
	start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE --exec $DAEMON
}

case "$1" in
  start)
	log_daemon_msg "Starting $DESC" "$DAEMON"
	do_start
	case "$?" in
		0) sendsigs_omit
		   log_end_msg 0 ;;
		1) log_progress_msg "already started"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac
	;;

  stop)
	log_daemon_msg "Stopping $DESC" "$DAEMON"
	do_stop
	case "$?" in
		0) log_end_msg 0 ;;
		1) log_progress_msg "already stopped"
		   log_end_msg 0 ;;
		*) log_end_msg 1 ;;
	esac
	;;

  restart|force-reload)
	$0 stop
	$0 start
	;;


  status)
	status_of_proc -p $PIDFILE $DAEMON romp && exit 0 || exit $?
	;;

  *)
	echo "Usage: $SCRIPTNAME {start|stop|restart|status}" >&2
	exit 3
	;;
esac

:
